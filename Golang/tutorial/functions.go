package tutorial

import "fmt"

func Functions() {
	myMessage()

	familyName("Anja")
	familyName("Reses")

	familyName1("Jenny", 30)

	fmt.Println(myFunction(1, 3))
	fmt.Println(myFunction2(3, 4))

	// store return value in variable
	total := myFunction2(4, 5)
	fmt.Println(total)

	// return two values
	fmt.Println(myFunction3(5, "Halo"))
	// use one return value
	_, b := myFunction3(10, "Hay")
	fmt.Println(b)
	a, _ := myFunction3(20, "T")
	fmt.Println(a)

	// recursion
	testcount(1)
	factor := factorial_recursion(4)
	fmt.Println(factor)
}

func myMessage() {
	fmt.Println("I just got executed!")
}

// parameter and arguments
func familyName(fname string) {
	fmt.Println("Hello", fname, "Refsnes")
}

// multiple parameter
func familyName1(fname string, age int) {
	fmt.Println("Hello", age, "year old", fname, "Refsnes")
}

// return values
func myFunction(x int, y int) int {
	return x + y
}

// named return values
func myFunction2(x int, y int) (result int) {
	result = x * y
	return
}

// multiple return value
func myFunction3(x int, y string) (result int, txt1 string) {
	result = x + x
	txt1 = y + " World!"
	return
}

/*
	Recursion
*/

func testcount(x int) int {
	if x == 11 {
		return 0
	}
	fmt.Println(x)
	return testcount(x + 1)
}

func factorial_recursion(x float64) (y float64) {
	if x > 0 {
		y = x * factorial_recursion(x-1)
	} else {
		y = 1
	}
	return
}
