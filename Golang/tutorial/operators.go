package tutorial

import "fmt"

func Operators() {
	var a = 15 + 25
	fmt.Println(a)

	var (
		sum1 = 100 + 50
		sum2 = sum1 + 250
		sum3 = sum2 + sum2
	)

	fmt.Println(sum3)

	// Arithmetic Operators
	// +, -, *, /, %, ++, --

	// Assignment Operators
	// =, +=, -=, *=, /=, %=, &=, |=, ^=, >>=, <<=
	var x = 10
	fmt.Println(x)
	x += 5
	fmt.Println(x)

	// Comparison Operators
	// ==, !=, >, <, >=, <=
	var z = 5
	var y = 3
	fmt.Println(z > y)

	// Logical Operators
	// &&, ||, !

	// Bitwise Operators
	// &, |, ^, <<, >>
}
