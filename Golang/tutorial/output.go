package tutorial

import "fmt"

func Output() {
	// print
	var i, j string = "Hello", "World"
	fmt.Print(i)
	fmt.Print(j)

	// print in new lines
	fmt.Print(i, "\n")
	fmt.Print(j, "\n")

	// printing multiple variablees
	fmt.Print(i, "\n", j)
	fmt.Print(i, " ", j)

	// print insert a space between the argument if neither are strings
	var k, l = 10, 20
	fmt.Print(k, l)

	// println
	fmt.Println(i, j)

	// printf - format argument
	/**
	General formatting
	%v - for value
	%T - for type
	%#f - value in Go-syntax format
	%% - % sign

	*/
	fmt.Printf("i has value : %v and type: %T\n", i, i)
	fmt.Printf("j has value : %v and type: %T\n", j, j)
	fmt.Printf("%#v\n", i)
	fmt.Printf("%v%%\n", i)
	fmt.Println()

	/**
	Integer formatting
	%b - base 2
	%d - base 10
	%+d - base 10 and always show sign
	%o - base 8
	%x - base 16 lowercase
	%X - base 16 uppercase
	%#x - base 16 with leading 0x
	%4d - pad with spaces (width 4, right justified)
	%-4d - pad with spaces (width4, left justified)
	%04d - pad with zeroes (width 4)
	*/
	a := 12
	fmt.Printf("%b\n", a)
	fmt.Printf("%d\n", a)
	fmt.Printf("%+d\n", a)
	fmt.Printf("%o\n", a)
	fmt.Printf("%O\n", a)
	fmt.Printf("%x\n", a)
	fmt.Printf("%X\n", a)
	fmt.Printf("%#x\n", a)
	fmt.Printf("%4d\n", a)
	fmt.Printf("%-4d\n", a)
	fmt.Printf("%04d\n", a)

	/*
		Strings formattings
		%s - plain string
		%q - double-quoted string
		%8s - plain string(width 8, right justified)
		%-8s - plain string(width 8, left justified)
		%x - hex dump of byte
		% x - hex dump with spaces
	*/

	str := "Hello"
	fmt.Printf("%s\n", str)
	fmt.Printf("%q\n", str)
	fmt.Printf("%8s\n", str)
	fmt.Printf("%-8s\n", str)
	fmt.Printf("%x\n", str)
	fmt.Printf("% x\n", str)

	// %t - boolean formatting
	z := false
	fmt.Printf("%t\n", z)
	fmt.Println()

	/*
		Floating formatting
		%e - scientific notation with 'e' as exponent
		%f - decimal point, no exponent
		%.2f - default width, precision 2
		%6.2f - width 6, precision 2
		%g - exponent as needed
	*/
	za := 3.411
	fmt.Printf("%e\n", za)
	fmt.Printf("%f\n", za)
	fmt.Printf("%.2f\n", za)
	fmt.Printf("%6.2f\n", za)
	fmt.Printf("%g\n", za)
}
