package tutorial

import "fmt"

// declaring untyped constant
const PI = 3.14

// declarig typed constant
const A string = "this is constant"

func Go_constant() {
	fmt.Println(PI)
	fmt.Println(A)

	// A = 2 -> error

	// multiple constant declaration
	const (
		D int = 10
		B     = 3.14
		C     = "Hi!"
	)

	fmt.Println(D)
	fmt.Println(B)
	fmt.Println(C)
}
