package tutorial

import "fmt"

func Go_Slices() {
	mySlice1 := []int{}
	fmt.Println(len(mySlice1))
	fmt.Println(cap(mySlice1))
	fmt.Println(mySlice1)

	mySlice2 := []string{"Go", "Slices", "Are", "Powerfull"}
	fmt.Println(len(mySlice2))
	fmt.Println(cap(mySlice2))
	fmt.Println(mySlice2)

	// create slice from an array
	arr1 := [6]int{10, 11, 22, 33, 44, 55}
	myslice3 := arr1[2:4]

	fmt.Printf("myslice = %v\n", myslice3)
	fmt.Printf("length = %d\n", len(myslice3))
	fmt.Printf("capacity = %d\n", cap(myslice3))

	// create slice with make function
	myslice4 := make([]int, 5, 10)
	fmt.Printf("myslice4 = %v\n", myslice4)
	fmt.Printf("length = %d\n", len(myslice4))
	fmt.Printf("capacity = %d\n", cap(myslice4))

	// create slice with make function and omitted capacity
	myslice5 := make([]int, 5)
	fmt.Printf("myslice5 = %v\n", myslice5)
	fmt.Printf("length = %d\n", len(myslice5))
	fmt.Printf("capacity = %d\n", cap(myslice5))
	fmt.Println()

	// append elements to a slice
	myslice6 := []int{1, 2, 3, 4, 5, 6}
	fmt.Printf("myslice6 = %v\n", myslice6)
	fmt.Printf("length = %d\n", len(myslice6))
	fmt.Printf("capacity = %d\n", cap(myslice6))

	myslice6 = append(myslice6, 20, 22, 13)
	fmt.Printf("myslice6 = %v\n", myslice6)
	fmt.Printf("length = %d\n", len(myslice6))
	fmt.Printf("capacity = %d\n", cap(myslice6))

	// append one slice to another slice
	myslice7 := []int{1, 2, 3}
	myslice8 := []int{4, 5, 6}
	myslice9 := append(myslice7, myslice8...)
	fmt.Printf("myslice9 = %v\n", myslice9)
	fmt.Printf("length = %d\n", len(myslice9))
	fmt.Printf("capacity = %d\n", cap(myslice9))

	// change the length or slice
	arr2 := [6]int{9, 8, 7, 6, 5, 4}
	mySlice10 := arr2[1:5] // slice array
	fmt.Printf("myslice10 = %v\n", mySlice10)
	fmt.Printf("length = %d\n", len(mySlice10))
	fmt.Printf("capacity = %d\n", cap(mySlice10))

	mySlice10 = arr2[1:3] // change length by re-slicing the array
	fmt.Printf("myslice10 = %v\n", mySlice10)
	fmt.Printf("length = %d\n", len(mySlice10))
	fmt.Printf("capacity = %d\n", cap(mySlice10))

	mySlice10 = append(mySlice10, 20, 12, 22, 34) // change length by appending items
	fmt.Printf("myslice10 = %v\n", mySlice10)
	fmt.Printf("length = %d\n", len(mySlice10))
	fmt.Printf("capacity = %d\n", cap(mySlice10))
	fmt.Println()

	// Memory efficiency
	numbers := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 3, 14, 15}
	// original slice
	fmt.Printf("numbers = %v\n", numbers)
	fmt.Printf("length = %d\n", len(numbers))
	fmt.Printf("capacity = %d\n", cap(numbers))

	// create copy with only needed numbers
	neededNumbers := numbers[:len(numbers)-10]
	numbersCopy := make([]int, len(neededNumbers))
	copy(numbersCopy, neededNumbers)

	fmt.Printf("numbersCopy = %v\n", numbersCopy)
	fmt.Printf("length = %d\n", len(numbersCopy))
	fmt.Printf("capacity = %d\n", cap(numbersCopy))
}
