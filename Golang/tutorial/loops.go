package tutorial

import "fmt"

func Loops() {
	// use for
	for i := 0; i < 100; i += 10 {
		fmt.Println(i)
	}

	for i := 0; i < 5; i++ {
		if i == 3 {
			continue
		}
		if i == 2 {
			break
		}
		fmt.Println(i)
	}

	// nested loops
	adj := [2]string{"big", "tasty"}
	fruits := [3]string{"apple", "orange", "banana"}
	for i := 0; i < len(adj); i++ {
		for j := 0; j < len(fruits); j++ {
			fmt.Println(adj[i], fruits[j])
		}
	}

	// use range
	fruits = [3]string{"apple", "orange", "banana"}
	for idx, val := range fruits {
		fmt.Printf("%v\t%v\n", idx, val)
	}

	for _, val := range fruits {
		fmt.Printf("%v\n", val)
	}

	for idx := range fruits {
		fmt.Printf("%v\n", idx)
	}
}
