package tutorial

import "fmt"

func Go_Array() {
	var arr1 = [3]int{1, 2, 4}
	arr2 := [5]int{6, 6, 7, 2, 9}

	// declared with infered length
	var arr3 = [...]string{"My boy", "You good"}
	arr4 := [...]int{9, 1, 4, 8}

	fmt.Println(arr1)
	fmt.Println(arr2)
	fmt.Println(arr4)
	fmt.Println(arr3)

	//access elements of an Array
	prices := [3]int{10, 20, 40}
	fmt.Println(prices[0])
	fmt.Println(prices[2])
	// change elements of an array
	prices[2] = 50
	fmt.Println(prices[2])

	//array initialized
	arr5 := [5]int{}
	arr6 := [5]int{1, 2}
	arr7 := [5]int{1, 2, 3, 4, 5}

	fmt.Println(arr5)
	fmt.Println(arr6)
	fmt.Println(arr7)

	// initialize only specific elements
	arr8 := [5]int{1: 10, 3: 40}
	fmt.Println(arr8)

	// find length
	fmt.Println(len(arr8))
}
