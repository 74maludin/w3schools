// This is a comment
package tutorial

import "fmt"

func Comment() {
	// This is a comment

	/* The code below will print Hello World
	to the screen, and it is amazing */
	fmt.Println("Hello from Comment!") // This is also comment

	/* comment to prevent code execution */
	// fmt.Println("This line does not execute")
}
