package tutorial

import "fmt"

func Conditions() {
	// if
	if 20 > 18 {
		fmt.Println("20 is greater than 18")
	}
	x := 20
	y := 18
	if x > y {
		fmt.Println("x is greater than y")
	}

	// if else
	time := 20
	if time < 18 {
		fmt.Println("Good day.")
	} else {
		fmt.Println("Good evening.")
	}
	temperature := 14
	if temperature > 15 {
		fmt.Println("It is warm out there")
	} else {
		fmt.Println("It is cold out there")
	}

	// else if
	a := 14
	b := 14
	if a < b {
		fmt.Println("Good morning.")
	} else if a == b {
		fmt.Println("Good day.")
	} else {
		fmt.Println("Good evening.")
	}

	// nested if
	num := 20
	if num >= 10 {
		fmt.Println("Num is more than 10.")
		if num > 15 {
			fmt.Println("Num is also more than 15.")
		}
	} else {
		fmt.Println("Num is less than 10.")
	}

	// switch
	day := 8
	switch day {
	case 1:
		fmt.Println("Monday")
	case 2:
		fmt.Println("Tuesday")
	case 3:
		fmt.Println("Wednesday")
	case 4:
		fmt.Println("Thursday")
	case 5:
		fmt.Println("Friday")
	case 6:
		fmt.Println("Saturday")
	case 7:
		fmt.Println("Sunday")
	default:
		fmt.Println("Not a Weekday")
	}

	// multi-case switch
	thisDay := 5
	switch thisDay {
	case 1, 3, 5:
		fmt.Println("Odd weekday")
	case 2, 4:
		fmt.Println("Even weekday")
	case 6, 7:
		fmt.Println("Weekend")
	default:
		fmt.Println("Invalid day of day number")
	}
}
