package tutorial

import "fmt"

func Maps() {
	// store data values in key:value pairs

	var a = map[string]string{
		"brand": "Ford",
		"model": "Mustang",
		"year":  "1964",
	}
	b := map[string]int{
		"Oslo":      1,
		"Bergen":    2,
		"Trondheim": 3,
		"Stavanger": 4,
	}

	fmt.Printf("a\t%v\n", a)
	fmt.Printf("b\t%v\n", b)

	// creating maps using make()
	var c = make(map[string]string)
	c["brand"] = "Mercedes"
	c["model"] = "AMG"
	c["year"] = "2006"

	d := make(map[string]int)
	d["Oslo"] = 1
	d["Bergen"] = 2

	fmt.Printf("c\t%v\n", c)
	fmt.Printf("d\t%v\n", d)

	// creating empty map
	var e = make(map[string]string)
	var f map[string]string

	fmt.Println(e == nil)
	fmt.Println(f == nil)
	fmt.Println(e)
	fmt.Println(f)

	// accessing map elements
	var ab = make(map[string]string)
	ab["brand"] = "Ford"
	ab["model"] = "Mustang"
	ab["year"] = "1964"

	fmt.Println(ab)
	// updating and adding map elements
	ab["brand"] = "Mercedes" // updating an element
	ab["color"] = "red"      // adding an element
	fmt.Println(ab)

	// remove element from map
	delete(ab, "year")
	fmt.Println(ab)

	// check for specific elements in a map
	val1, ok1 := ab["brand"] // checking for existing key and its value
	val2, ok2 := ab["year"]  // checking for non-existing key and its value
	val3, ok3 := ab["day"]
	_, ok4 := ab["model"]

	fmt.Println(val1, ok1)
	fmt.Println(val2, ok2)
	fmt.Println(val3, ok3)
	fmt.Println(ok4)
	fmt.Println()

	// Maps Are References
	var abc = map[string]string{"brand": "Toyota", "model": "Ayla", "year": "2012"}
	cba := abc
	fmt.Println(abc)
	fmt.Println(cba)

	cba["year"] = "2001"
	fmt.Println("After change to b:")

	fmt.Println(abc)
	fmt.Println(cba)
	fmt.Println()

	// iterating over maps
	numbers := map[string]int{"one": 1, "two": 2, "three": 3, "four": 4}

	for k, v := range numbers {
		fmt.Printf("%v : %v\n", k, v)
	}
	fmt.Println()

	var numbersA []string // defining the order
	numbersA = append(numbersA, "one", "two", "three", "four") 

	for k, v := range numbers { // loop with no order
		fmt.Printf("%v : %v\n", k, v)
	}
	fmt.Println()

	for _, element := range numbersA { // loop with the defined order
		fmt.Printf("%v : %v\n", element, numbers[element])
	}
}

/*
	Allowed Key Types
	- booleans
	- numbers
	- strings
	- arrays
	- pointers
	- structs
	- interfaces
	Invalid
	- slices
	- maps
	- functions
*/
