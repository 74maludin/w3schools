package tutorial

import "fmt"

func DataTypes() {
	// basic data types: bool, numeric, string
	var a bool = true       // bool
	var b int = 5           // signed integer
	var f int = -4500       // signed integer
	var g uint = 500        // unsigned integer
	var c float32 = 3.14    // floating point number
	var i float64 = 3.4e+28 // floating point number
	var d string = "Hi!"    // string

	fmt.Println("Boolean: ", a)
	fmt.Println("Signed Integer: ", b)
	fmt.Println("Signed Integer: ", f)
	fmt.Println("Unsigned Integer: ", g)
	fmt.Println("Float 32: ", c)
	fmt.Println("Float 64: ", i)
	fmt.Println("String: ", d)

	// int: int8, int16, int, int32, int64
	// float: float32, float64
}
