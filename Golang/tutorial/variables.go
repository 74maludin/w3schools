package tutorial

import "fmt"

/* declare variable outside function */
var d int
var e int = 2
var f = 3

// g := 12 -> this will get error non-declaration statement outside function body

func DeclaringVariable() {
	/* Declaring variable with initial value */
	var student1 string = "John" // type is string
	var student2 = "Jane"        // type is inferred
	x := 2                       // type is inferred

	fmt.Println(student1)
	fmt.Println(student2)
	fmt.Println(x)
	fmt.Println()

	/* Declaring variable without initial value */
	var a string
	var b int
	var c bool

	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println()

	/* Value assignment after declaration */
	// var student1 string -> this will get error
	student1 = "Jams"
	fmt.Println(student1)
	fmt.Println()

	/* declaring outside function */
	d = 1
	fmt.Println(d)
	fmt.Println(e)
	fmt.Println(f)
	// fmt.Println(g)
	fmt.Println()

	/* Multiple Variable Declaration */
	var h, i, j, k int = 4, 5, 7, 8
	l, m := 7, "Strings"

	fmt.Println(h)
	fmt.Println(i)
	fmt.Println(j)
	fmt.Println(k)
	fmt.Println(l)
	fmt.Println(m)
	fmt.Println()

	/* Variable declaration in a block */
	// for more readability
	var (
		n int
		o int    = 1
		p string = "hello"
	)

	fmt.Println(n)
	fmt.Println(o)
	fmt.Println(p)
	fmt.Println()

	/* Naming rules */
	// Camel Case
	myVariableName := "Joshua"
	// Pascal Case
	var MyVariableName string = "Jemie"
	// Snake Case
	my_variable_name := "Juara"
	fmt.Println(myVariableName)
	fmt.Println(MyVariableName)
	fmt.Println(my_variable_name)

}
