## Go Tutorial

This tutorial based on [Go Tutorial w3schools](https://www.w3schools.com/go/index.php)

learning at September 11th 2022.

### Table of Contents

- [Material](#material)
- [Introduction](#introduction-hello-world)
- [Go Syntax](#go-syntax)

#### Material

- Comments
- Variable
- Constants
- Output
- Arrays
- Slices
- Operators
- Conditions
- Switch
- Loops
- Functions
- Struct
- Maps

#### Introduction (Hello World)

- open source, syntax similar to C++
- used for:
  - high performance applications
  - web development
  - network based program
  - cross platform enterprise applications
  - cloud native development
- fast, static type, compiled language
- developed at Google by Robert Griesemer, Rob Pike, and Ken Thompson
- why go
  - fun, easy to learn
  - fast runtime and compilation time
  - supports concurrency
  - has memory management
  - works on different platforms (Windows, Mac, Linux, Raspberry, etc)
- Installing Go extension on VSCode
  - Install Go on Extension Marketplace
  - CTRL + SHIFT + P => `Go: Install/Update Tools`

#### Go syntax

Go file consists:

- Package declaration
- Import packages
- Functions
- Statements and expressions
