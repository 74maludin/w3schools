package main

import (
	"fmt"
	"go_tutorial/tutorial"
)

func main() {
	tutorial.Hello()
	tutorial.Comment()
	fmt.Println()

	tutorial.DeclaringVariable()
	fmt.Println()

	tutorial.Go_constant()
	fmt.Println()

	tutorial.Output()
	fmt.Println()

	tutorial.DataTypes()
	fmt.Println()

	tutorial.Go_Array()
	fmt.Println()

	tutorial.Go_Slices()
	fmt.Println()

	tutorial.Operators()
	fmt.Println()

	tutorial.Conditions()
	fmt.Println()

	tutorial.Loops()
	fmt.Println()

	tutorial.Functions()
	fmt.Println()

	tutorial.Structure()
	fmt.Println()

	tutorial.Maps()
}
