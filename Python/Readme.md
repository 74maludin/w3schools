## Python Tutorial

This tutorial based on [Python Tutorial w3schools](https://www.w3schools.com/python/default.asp)

### Table of Contents

- [Tutorial](#tutorial)
- [File Handling](#file-handling)
- [Modules](#modules)
- [Matplotlib](#matplotlib)
- [Machine Learning](#machine-learning)
- [Database](#database)
- [Introduction](#introduction)
- [Opreator groups](#operator-groups)

#### Tutorial

- Syntax
- Comments
- Variables
- Data Types
- Numbers
- Casting
- Strings
- Booleans
- Operators
- Lists
- Tuples
- Sets
- Dictionaries
- If...Else
- While Loops
- For Loops
- Functions
- Lambda
- Arrays
- Classes/Objects
- Inheritance
- Iterators
- Scope
- Modules
- Dates
- Math
- JSON
- RegEx
- PIP
- Try...Except
- User Input
- String Formatting

Learning at September, 18th 2022.

#### File Handling

- File Handling
- Read Files
- Write/Create Files
- Delete Files

#### Modules

- NumPy
- Pandas
- SciPy
- Django

#### Matplotlib

- Pyplot
- Plotting
- Markers
- Line
- Labels
- Grid
- Subplot
- Scatter
- Bars
- Histograms
- Pie Charts

#### Machine Learning

- Mean Median Mode
- Standard Deviation
- Percentile
- Data Distribution
- Normal Data Distribution
- Scatter Plot
- Linear Regression
- Polynomial Regression
- Multiple Regresssion
- Scale
- Train/Test
- Decision Tree
- Confusion Matrix
- Hierarchical Clustering
- Logistic Regression
- Grid Search
- Categorical Data
- K-Means
- Boostrap Agregation
- Cross Validation
- AUC - ROC Curve
- K-nearest neighbors

#### Database

| MySQL           | MongoDB           |
| --------------- | ----------------- |
| Create Database | Create Database   |
| Create Table    | Create Collection |
| Insert          | Insert            |
| Select          | Find              |
| Where           | Query             |
| Order By        | Sort              |
| Delete          | Delete            |
| Drop Table      | Drop Collection   |
| Limit           | Limit             |
| Join            | -                 |
| -               | Update            |

#### Introduction

- created by Guido van Rossum, released 1991
- used for
  - web development (server side), web applications
  - software development
  - mathematics
  - system scripting
- Syntax
  - similarities to the English with influence from mathematics
  - uses new lines to complete a command
  - relies on indentation, using whitespace

#### Operator groups

- Arithmetic operators
- Assignment operators
- Comparison operators
- Logical operators
  - `and`, `or`, `not`
- Identity operators
  - `is`, `is not`
- Membership operators
  - `in`, `not in`
- Bitwise operators
