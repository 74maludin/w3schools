x = 5
y = "John"
print(x)
print(y)

# type casting
x = str(3)
y = int(3)
z = float(3)

print(x)
print(y)
print(z)

# string can declared with single or double quote
x = "John"  # as same as
y = 'John'
print(x)
print(y)

# case sensitive
a = 4
A = 5
print(A)
print(a)
print()

# multi words variable names
# Camel Case
myVariableName = "Camel"
# Pascal Case
MyVariableName = "Pascal"
# Snake Case
my_variable_name = "Snake"

print(myVariableName)
print(MyVariableName)
print(my_variable_name)
print()

# Assign multiple values
x, y, z = "Orange", "Banana", "Cherry"
print(x)
print(y)
print(z)

# One value to a multiple variables
x = y = z = "Orange"
print(x)
print(y)
print(z)

# unpack collection
fruits = ["apple", "banana", "cherry"]
x, y, z = fruits
print(x)
print(y)
print(z)
print()

# Output variables
x = "Python is awesome"
print(x)

x = "Python1"
y = "is1"
z = "awesome1"
print(x, y, z)

x = "Python2"
y = "is2"
z = "awesome2"
print(x + y + z)
print()

# Global variables
x = "awesome"


def myFunc():
    x = "fantastic"
    print("Python is " + x)


myFunc()
print("Python is " + x)

# use global


def myFunc2():
    global x
    x = "fantastis 2 "


myFunc2()
print("Python is " + x)
