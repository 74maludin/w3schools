# creating function
from fnmatch import fnmatch


def my_function():
    print("Hello from a function")


# calling a function
my_function()

# arguments


def my_function(fname):
    print(fname + " Refsnes")


my_function("Emil")

# number of arguments


def my_function(fname, lname):
    print(fname + " " + lname)


my_function("Emil", "Sahara")

# arbitrary arguments


def my_function(*kids):
    print("The youngest child is " + kids[2])


my_function("Emil", "Tobias", "Linus")

# keyword arguments


def my_function(child2, child1, child3):
    print("The youngest child is " + child3)


my_function(child1="Emil", child2="Tobias", child3="Luna")

# aribitrary keyword **kwargs


def my_function(**kid):
    print("His last name is " + kid["lname"])


my_function(fname="Tobias", lname="Refness")

# default parameter value


def my_function(country="Norway"):
    print("I am from " + country)


my_function()
my_function("Sweden")

# passing a list as an argument


def my_function(food):
    for x in food:
        print(x)


fruits = ["apple", "banana", "cherry"]
my_function(fruits)

# return values


def my_function(x):
    return 5 * x


print(my_function(3))

# pass


def myfunction():
    pass

# Recursion


def tri_recursion(k):
    if (k > 0):
        result = k + tri_recursion(k - 1)
        print(result)
    else:
        result = 0
    return result


print("\n\nRecursion Example result")
tri_recursion(6)
print()

# Lambda
x = lambda a : a + 10
print(x(5))
x = lambda a, b, c : a + b + c
print(x(5, 6, 2))

# use Lambda Functions
def myfunc(n):
  return lambda a : a * n

mydoubler = myfunc(2)
mytripler = myfunc(3)

print(mydoubler(11))
print(mytripler(11))
print()
