# store multiple items in a single variable
# collection unordered, unchangeable*, unindexed

import this


thisset = {"apple", "banana", "cherry"}
print(thisset)

# duplicate not allowed
thisset = {"apple", "banana", "cherry", "apple"}
print(thisset)
print(type(thisset))
for x in thisset:
    print(x)
print("apple" in thisset)
print("mango" in thisset)

# add set items
thisset.add("orange")
print(thisset)
# add from another sets
tropical = {"pineaple", "mango", "papaya"}
thisset.update(tropical)
print(thisset)
# add list
mylist = ["kiwi", "watermelon"]
thisset.update(mylist)
print(thisset)
# remove item
thisset.remove("banana")
print(thisset)
# remove with discard
thisset.discard("mango")
print(thisset)
thisset.pop()
print(thisset)
thisset.clear() # or del thisset
print(thisset)
print()

# Loop sets
thisset = {"apple", "mango", "cherry"}
for x in thisset:
  print(x)
print()

# join sets
set2 = {1, 2, 4}
set3 = thisset.union(set2)
print(set3)
thisset.update(set2)
print(thisset)

# intersection duplicates
thisset.intersection_update(set3)
print(thisset)
z = thisset.intersection(set2)
print(z)
thisset.symmetric_difference_update(set2)
print(thisset)
z1 = thisset.symmetric_difference(set2)
print(z1)