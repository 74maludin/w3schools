import this


thisdict = {
    "brand": "Ford",
    "model": "Mustang",
    "year": 1964,
    "colors": ["red", "white", "blue"]
}
print(thisdict)

# dictionaries are used to store data values in key: value pairs
print(type(thisdict))
print()

# access dictionary items
print(thisdict["brand"])
x = thisdict.get("model")
print(x)
# get list of the keys
x = thisdict.keys()
print(x)
thisdict["engine"] = "v4"
print(x)
# get list of the values
x = thisdict.values()
print(x)
print(type(x))
x = thisdict.items()
print(x)
if "model" in thisdict:
    print("Yes, 'model' is one of the keys in the thisdict dictionary")
print()

# change dictionary items
thisdict["year"] = 2018
print(thisdict)
thisdict.update({"model": "Chevrolet"})
print(thisdict)
print()

# add dictionary items
thisdict["window"] = "overseas"
print(thisdict)
thisdict.update({"from": "USA", "city": "Arkansas"})
print(thisdict)
print()

# remove item
thisdict.pop("window")
print(thisdict)
thisdict.popitem()
print(thisdict)
del thisdict["engine"]
print(thisdict)
# thisdict.clear()
for x in thisdict:
  print(thisdict[x])
for x in thisdict.values():
  print(x)
for x in thisdict.keys():
  print(x)
for x, y in thisdict.items():
  print(x, y)
print()

# Copy Dictionaries
mydict = thisdict.copy()
print(mydict)
print(type(mydict))
mydict2 = dict(thisdict)
print(mydict2)

# Nested dictionaries
myfamily = {
  "child1": {
    "name": "Emil",
    "year": 2004
  },
  "child": {
    "name": "Tobias",
    "year": 2007
  },
  "child3": {
    "name": "Linus",
    "year": 2011
  }
}