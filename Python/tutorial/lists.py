from traceback import print_tb


mylist = ["apple", "banana", "cherry", "mango", "apple"]
print(mylist)
print(len(mylist))

# list items
# ordered, changeable, allow duplicate values

list1 = ["abc", 34, True, 40, "male"]
print(list1)
print(type(mylist))
print(isinstance(mylist, list))

list2 = list(("apple", "banana", "cherry"))  # double round-brackets
print(list2)

# access items
print(list2[1])
print(list2[-1])
print(list2[:1])

if "apple" in list2:
    print("Yes, 'apple' is in the fruits list")

# change item value
list2[1] = "blackcurrant"
print(list2)
list2[1:] = ["watermelon", "papaya"]
print(list2)
print(list2[1:3])

# add list items
list2.insert(1, "kedondong")
print(list2)
list2.append("orange")
print(list2)
list2.extend(list1)
print(list2)
list2.extend(("kiwi", "mango"))
print(list2)

# remove
list2.remove("abc")
print(list2)
# remove specific index
list2.pop(2)
print(list2)
del list2[1]
print(list2)
# list2.clear()
# print(list2)
print()

# Loop
for x in list2:
    print(x)
# print with index number
for i in range(len(list2)):
    print(list2[i])
# using while loops
i = 0
while i < len(list2):
    print(list2[i])
    i += 1

# using list comprehension
print()
[print(x) for x in list2]
newlist = [x for x in list2 if x != "apple"]
print(newlist)
[print(str(x).upper()) for x in list2]
print()
newlist = [str(x) if x != "papaya" else "rambutan" for x in list2]
print(newlist)

# sort list
# case sensitive
newlist.sort(reverse=True)
print(newlist)
newlist.sort(key=str.lower)
print(newlist)
# reverse
newlist.reverse()
print(newlist)
print()

# Copy List
newlist_2 = newlist.copy()
print(newlist_2)
# copy with built in list
newlist_2 = list(newlist)
print(newlist_2)
print()

# Join lists
list3 = newlist + newlist_2
list3.sort(reverse=True)
print(list3)
list1 = ["a", "b", "c"]
list2 = [1, 2, 3]
for x in list2:
    list1.append(x)
print(list1)
list1.extend(list2)
print(list1)
