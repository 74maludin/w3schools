import json

# some JSON
x = '{"name": "John", "age": 30, "city": "New York"}'

# parse x
y = json.loads(x)

print(y["age"])

x = {
    "name": "Abang",
    "age": 35,
    "city": "Washington"
}

# convert into JSON
y = json.dumps(x)
print(y)
print(json.dumps({"name": "John", "age": 30}))
print(json.dumps(["apple", "bananas"]))
print(json.dumps(("apple", "banana")))
print(json.dumps("hello"))
print(json.dumps(42))
print(json.dumps(31.76))
print(json.dumps(True))
print(json.dumps(None))
print()

x = {
    "name": "John",
    "age": 30,
    "married": True,
    "divorced": True,
    "children": ("Ann", "Bilyy"),
    "pets": None,
    "cars": [
        {"model": "BMW 230", "mpg": 27.5},
        {"model": "Ford Edge", "mpg": 24.1}
    ]
}
print(json.dumps(x, indent=2))
print()
print(json.dumps(x, indent=2, sort_keys=True))
