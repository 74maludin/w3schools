# Python data types

from traceback import print_tb
from unicodedata import name


built_in_data_types = {
    "text": "str",
    "numeric": ["int", "float", "complex"],
    "sequence": ["list", "tuple", "range"],
    "mapping": "dict",
    "set": ["set", "frozenset"],
    "boolean": "bool",
    "binary": ["bytes", "bytearrary", "memoryview"],
    "none": "NoneType"
}

x = 5
print(type(x))

# setting specific data types
x = str("Hello World")
print('str', x)
x = int(20)
print('int', x)
x = float(20.5)
print('float', x)
x = complex(1j)
print('complex', x)
x = list(("apple", "banana", "cherry"))
print('list', x)
x = tuple(("apel", "pisang", "cery"))
print('tuple', x)
x = range(6)
print('range', x)
x = dict(name="John", age=36)
print('dict', x)
x = set(("apple", "banana", "cherry"))
print('set', x)
x = frozenset(("apple", "banana", "cherry"))
print('frozenset', x)
x = bool(5)
print('bool', x)
x = bytes(5)
print('bytes', x)
x = bytearray(5)
print('bytearray', x)
x = memoryview(bytes(5))
print('memoryview', x)