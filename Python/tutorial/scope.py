def myfunc():
  x = 300
  print(x)

myfunc()

# function inside function

def myfunc():
  x = 300
  def myinnerfunc():
    print(x)
  myinnerfunc()

myfunc()

# global scope
x = 400
def myfunc():
  global x
  x = 200
  print(x)
myfunc()
print(x)