x = 1
y = 2.8
z = 1j

print(type(x))
print(type(y))
print(type(z))
print()

x = 1
y = 35622225548887711
z = -325522

print(type(x))
print(type(y))
print(type(z))
print()

x = 1.10
y = 1.0
z = -35.59

print(type(x))
print(type(y))
print(type(z))

# this also float
x = 35e3
y = 12E4
z = -87.7e100

print(type(x), x)
print(type(y), y)
print(type(z), z)
print()

x = 3 + 5j
y = 5j
z = -5j

print(type(x), x)
print(type(y), y)
print(type(z), z)
print()

# Type convertion
x = 1
y = 2.8
z = 1j
print(x, y, z)

a = float(x)
b = int(y)
c = complex(x)
print(a, b, c)
print(type(x), type(b), type(c))

# random number
import random

print(random.randrange(2, 7))