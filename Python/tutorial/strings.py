print("Hello")

a = "Dunia"
print(a)

# multiline string
a = """Lorem
loremsagfaga
sdajfaro
"""
print(a)
a = '''
ini juga bisa
pakai singlequote
'''
print(a)

# string are arrays
a = "Hello, World!"
print(a[1])

# Looping through a string
for x in "mangga":
    print(x)

# string length
print(len(a))
print()

# check string
txt = "The best things in life are free!"
print("free" in txt)
if "free" in txt:
    print("Yes, 'free' is present.")

# check if not
print("expensive" not in txt)
print()

# Slicing
print(txt[2:7])
# slice from start
print(txt[:7])
# slice to end
print(txt[7:])
# negative indexing
print(txt[-7:-2])
print()

# Modify strings
# uppercase
print(a.upper())
# lowercase
print(a.lower())
# remove whitespace
print(a.strip())
# replace a string
print(a.replace("H", "J"))
# split string
print(a.split(","))

# string concatenation
a = "Hello"
b = "My World"
c = a + " " + b
print(c)
print()

# format strings
# cannot combine int and string
age = 36
# txt = "My name is John, I am " + 36 # return error

# use format()
text = "My name is John, and I am {}"
print(text.format(age))
quantity = 3
itemno = 567
price = 49.95
myorder = "I want {} pieces of item {} for {} dollars"
print(myorder.format(quantity, itemno, price))
myorder = "I want to pay {2} dollars for {0} pieces of item {1}"
print(myorder.format(quantity, itemno, price))
print()

# Escape characters
txt = "We are the so-called \"Vikings\" form the north."
print(txt)