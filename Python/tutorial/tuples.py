# tuple used to store multiple items in a single variable
from cgitb import grey
from itertools import count


thistuple = ("apple", "banana", "cherry")
print(thistuple)

# tuple are ordered, unchangeable, allow duplicate values

# allow duplicates
thistuple = ("apple", "banana", "apple", "banana")
print(thistuple)
print(len(thistuple))

# create table with one item
thistuple2 = ("apple",)
print(type(thistuple))
thistuple1 = ("apple")
print(type(thistuple))
print()

# access tuple items
print(thistuple[1])
# negative indexing
print(thistuple[-1])
# range tuple
print(thistuple[1:3])
# check tuple
if "apple" in thistuple:
  print("Yes, 'apple' here")

# change tuple values
x = ("apple", "banana", "cherry")
y = list(x)
y[1] = "kiwi"
x = tuple(y)
print(x)
y.append("orange")
print(tuple(y))
y.remove("apple")
print(tuple(y))
print()

# Unpack tupple
fruits = ("apple", "banana", "cherry", "strawberry", "raspberry")
(green, yellow, *red) = fruits
print(green, yellow, red)

# Loop tuples
for x in fruits:
  print(x)
for x in range(len(fruits)):
  print(x, fruits[x])
i = 0
while i < len(fruits):
  print(i+2, fruits[i])
  i+=1

# Join tuples
tuple1 = ("a", "b", "c")
tuple2 = (1,2,3, False)
tuple3 = tuple1 + tuple2
print(tuple3)
multy_tuple = tuple2 * 2
print(multy_tuple)
print(multy_tuple.count(1))