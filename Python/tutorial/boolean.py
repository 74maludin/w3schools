print(10 > 9)
print(10 == 9)
print(10 < 9)

a = 200
b = 33
if b > a:
  print("b is greater than a")
else:
  print("b is not greater than a")

# evaluate values and variables
print(bool("hello"))
print(bool(15))
print(bool(0))
print(bool(None))
print(bool(""), bool(()), bool([]), bool({}))
print()

class myCalss():
  def __len__(self):
    return 0
myobj = myCalss()
print(bool(myobj))

# function return a boolean
def myFunction():
  return True
print(myFunction())

if myFunction():
  print("Yes!")
else:
  print("No!")

x = 200
print(isinstance(x, int))