# While
i = 1
while i < 6:
    print(i)
    i += 1
print()

# break statement
i = 1
while i < 6:
    print(i)
    if i == 3:
        break
    i += 1
print()

# continue
i = 0
while i < 6:
    i += 1
    if i == 3:
        continue
    print(i)

# else
i = 1
while i < 6:
    print(i)
    i += 1
else:
    print("i is no longer less than 6")
print()

# For loops
fruits = ["apple", "banana", "cherry"]
for x in fruits:
    print(x)

# looping string
for x in "banana":
    print(x)

# break
for x in fruits:
    print(x)
    if x == "banana":
        break
print()

for x in range(6):
    print(x)
print()

for x in range(2, 6):
  print(x)
print()

for x in range(2, 30, 3):
  print(x)
else:
  print("Finally finished")

adj = ["red", "big", "tasty"]
for x in adj:
  for y in fruits:
    print(x, y)