class MyClass:
  x = 5

p1 = MyClass()
print(p1.x)

class Person:
  def __init__(self, name, age):
    self.name = name
    self.age = age
  
  def myfunc(abc):
    print("Hello my name is " + abc.name)
  
  def printname(self):
    print(self.name, self.age)

p1 = Person("John", 36)
p1.age = 40
print(p1.name)
print(p1.age)
del p1.age
p1.myfunc()
print()

# Inheritance
class Student(Person):
  def __init__(self, name, age, year):
    super().__init__(name, age)
    self.graduationyear = year

  def welcome(self):
    print("Welcome ", self.name, self.age, " to the class of ", self.graduationyear)

x = Student("Mike", 13, 2018)
x.printname()
x.welcome()